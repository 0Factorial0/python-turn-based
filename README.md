# Turn Based Testing
You choose a dungeon and face enemies of that dungeon. Then you leave at some point or die.

You get experience for that and level up. So you can unlock more dungeons and be more strong.

Eventually defeat the ending boss, clone of you.
## Dungeons
 - Skeleton Dungeon (Level 1)
 - Zombie Dungeon (Level 3)
 - PVP Dungeon (Level 5)
### Skeleton Dungeon [Easy]
Spawns 1 to 3 skeletons based on your level.
### Zombie Dungeon [Normal]
Spawns 1 to 3 zombies based on your level.
### PVP Dungeon [Hard]
Spawns 1 player exactly like you.

//0x0
