def menu(player1):
    import time

    #import additional functions
    try:
        from additional_functions.clear import clear
        from additional_functions.see_stats import see_stats
        from additional_functions.rest import rest
        from dungeons.skeleton_dungeon import skeleton_dungeon
        from dungeons.zombie_dunegon import zombie_dungeon
        from dungeons.pvp_dungeon import pvp_dungeon
    except:
        print("Import Error.")

    #clear screen
    clear()

    #print menu
    print("------------------------")
    print("Press 1 For Skeleton Dungeon")
    if player1.lvl >= 3:
        print("Press 2 For Zombie Dungeon")
    if player1.lvl >= 5:
        print("Press 3 For PVP Dungeon")
    print("Press 23 For Rest")
    print("Press 0 For Character Sheet")
    print("------------------------")

    #get input
    try:
        userinput0 = int(input("Input: ")) 
    except:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        menu(player1)
    print("------------------------")

    #check input and route
    if userinput0 == 0:
        see_stats(player1)
    elif userinput0 == 1:
        skeleton_dungeon(player1)
    elif userinput0 == 2 and player1.lvl >= 3:
        zombie_dungeon(player1)
    elif userinput0 == 3 and player1.lvl >= 5:
        pvp_dungeon(player1)
    elif userinput0 == 23:
        rest(player1)
    else:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        menu(player1)
