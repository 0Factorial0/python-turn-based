def pvp_dungeon_play(player1, player_win):
    import time
    import random
    
    #import additional functions
    try:
        from additional_functions.clear import clear
        from additional_functions.calculate_level import calculate_level
        from additional_functions.rest import rest
        from constructors.const_entity import const_entity
        from end import end
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #generate npc player
    player2 = const_entity("Boss", player1.lvl, player1.x, 0)

    player_win == False

    while player2.hp > 0:

        #clear screen
        clear()

        #print npc player
        print("------------------------")
        print("{0};".format(player2.name))
        print("------------------------")
        print(player2)

        #print player
        print("{0};".format(player1.name))
        print("------------------------")
        print(player1)
        
        #get input
        print("Input A To Attack")
        print("------------------------")
        try:
            userinput0 = str(input("Input: ")) 
        except:
            print("------------------------")
            print("Input Error.")
            print("------------------------")
            time.sleep(2)
            pvp_dungeon(player1)

        #check input validity
        if userinput0 != "A":
            print("------------------------")
            print("Input Error.")
            print("------------------------")
            time.sleep(2)
            pvp_dungeon(player1)

        #clear screen
        clear()

        player_damage = random.randint(1, player1.dice) - player2.defence
        npc_damage = random.randint(1, player2.dice) - player1.defence

        #calculate damage
        if player_damage < 0:
            player_damage = 0
        
        if npc_damage < 0:
            npc_damage = 0

        player1.hp -= npc_damage
        player2.hp -= player_damage

        #print damage
        print("------------------------")
        print("{0} Dealt {1} Damage To {2}".format(player1.name, player_damage, player2.name))
        print("------------------------")
        time.sleep(1)
        print("{0} Dealt {1} Damage To {2}".format(player2.name, npc_damage, player1.name))
        print("------------------------")

        time.sleep(2)

        #check if player is dead
        if player1.hp <= 0:
            print("You Dead")
            print("------------------------")
            time.sleep(2)
            rest(player1)

        #check if player won
        if player1.hp > 0 and player2.hp <= 0:
            player_win = True

        #if player won, then return to dungeon enterance
        if player_win == True:
            print("You Win")
            print("------------------------")
            end()

def pvp_dungeon(player1):
    import time

    #import additional functions
    try:
        from additional_functions.clear import clear
        from menu import menu
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #print menu
    print("------------------------")
    print("PVP Dungeon")
    print("------------------------")
    print("Press 1 For Yes")
    print("Press 0 For Return To Menu")
    print("------------------------")

    #get input
    try:
        userinput0 = int(input("Input: ")) 
    except:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        pvp_dungeon(player1)
    print("------------------------")

    #check input and route
    if userinput0 == 1:
        player_win = False
        pvp_dungeon_play(player1, player_win)
    elif userinput0 == 0:
        menu(player1)
    else:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        pvp_dungeon(player1)
