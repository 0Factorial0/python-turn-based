def getsummary(amount_of_zombies, zombies, dead_monster_amount, monster_max_hp):
    #calculate summary
    monster_hp_summary = 0
    for i in range(0, amount_of_zombies):
        monster_hp_summary += int(zombies[i].hp)
    monster_hp_summary -= dead_monster_amount*monster_max_hp
    return monster_hp_summary

def zombie_dungeon_play(player1, player_win):
    import time
    import random
    
    #import additional functions
    try:
        from additional_functions.clear import clear
        from additional_functions.calculate_level import calculate_level
        from additional_functions.write_stats import write_xp
        from additional_functions.rest import rest
        from constructors.const_entity import const_entity
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #how many zombies to generate
    amount_of_zombies = random.randint(1,3)

    #all zombies
    zombies = []

    #sum of monsters
    dead_monster_amount = 0

    #generate zombies
    for i in range(0, amount_of_zombies):
        zombies.append(const_entity("Zombie[{0}]".format(i+1), player1.lvl, 1, 0))

    max_hp = zombies[0].hp

    while dead_monster_amount < amount_of_zombies:
        
        monster_max_hp = max_hp
        monster_hp_summary = getsummary(amount_of_zombies, zombies, dead_monster_amount, monster_max_hp)

        #clear screen
        clear()

        #print zombies
        print("------------------------")
        print("Zombies;")
        print("------------------------")
        for j in range(0, amount_of_zombies):
            if zombies[j].hp <= 0:
                continue
            else:
                print(zombies[j])

        #print player
        print("{0};".format(player1.name))
        print("------------------------")
        print(player1)
        
        #menu
        print("Choose One To Attack 1-3")
        print("------------------------")

        #get input
        try:
            userinput0 = int(input("Input: ")) 
        except:
            print("------------------------")
            print("Input Error.")
            print("------------------------")
            time.sleep(2)
            zombie_dungeon(player1)

        #check input validity
        if userinput0 < 1 or userinput0 > amount_of_zombies:
            print("------------------------")
            print("Input Error.")
            print("------------------------")
            time.sleep(2)
            zombie_dungeon(player1)

        #clear screen
        clear()

        #generate damage
        cumulative_mob_damage = 0
        for k in range(0, amount_of_zombies):
            cumulative_mob_damage += random.randint(1, zombies[k].dice)
            time.sleep(0.1)
        player_damage = random.randint(1, player1.dice) - zombies[0].defence

        #calculate damage
        if player_damage < 0:
            player_damage = 0
        
        if cumulative_mob_damage < 0:
            cumulative_mob_damage = 0

        monster_hp_summary -= player_damage
        zombies[(userinput0)-1].hp -= player_damage
        cumulative_mob_damage -= player1.defence
        player1.hp -= cumulative_mob_damage

        #print damage
        print("------------------------")
        print("{0} Dealt {1} Damage To {2}".format(player1.name, player_damage, zombies[(userinput0)-1].name))
        print("------------------------")
        time.sleep(1)
        print("Zombies Dealt {0} Damage To {1}".format(cumulative_mob_damage, player1.name))
        print("------------------------")

        time.sleep(2)

        #print if any zombie is dead
        for l in range(0, amount_of_zombies):
            if zombies[l].hp <= 0 and zombies[l].exist == True:
                print("{0} Is Dead, Player Earned 2 XP".format(zombies[l].name))
                player1.xp += 2
                zombies[l].exist = False
                dead_monster_amount += 1
                write_xp(player1)
                print("------------------------")

        #check if player is dead
        if player1.hp <= 0:
            print("You Dead")
            print("------------------------")
            time.sleep(2)
            rest(player1)
            zombie_dungeon(player1)

        #check if player won
        if player1.hp > 0 and monster_hp_summary <= 0:
            player_win = True

        #if player won, then return to dungeon enterance
        if player_win == True:
            print("You Win")
            print("------------------------")
            time.sleep(2)
            #calculate level
            calculate_level(player1)
            rest(player1)
            zombie_dungeon(player1)

def zombie_dungeon(player1):
    import time

    #import additional functions
    try:
        from additional_functions.clear import clear
        from menu import menu
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #print menu
    print("------------------------")
    print("Zombie Dungeon")
    print("------------------------")
    print("Press 1 For Yes")
    print("Press 0 For Return To Menu")
    print("------------------------")

    #get input
    try:
        userinput0 = int(input("Input: ")) 
    except:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        zombie_dungeon(player1)
    print("------------------------")

    #check input and route
    if userinput0 == 1:
        player_win = False
        zombie_dungeon_play(player1, player_win)
    elif userinput0 == 0:
        menu(player1)
    else:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        zombie_dungeon(player1)
