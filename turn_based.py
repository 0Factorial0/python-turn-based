def turn_based():
    import time

    #import additional functions
    try:
        from additional_functions.clear import clear
        from additional_functions.write_stats import write_stats
        from additional_functions.load import load
        from constructors.const_entity import const_entity
        from menu import menu
    except:
        print("Import Error.")

    #clear screen
    clear()

    #get name
    print("------------------------")
    try:
        print("Input 0 If You Have A Saved Character")
        print("------------------------")
        userinput_name = str(input("Name: ")) 
    except:
        print("------------------------")
        print("Input Error.")
        print("------------------------")
        time.sleep(2)
        turn_based()
    print("------------------------")

    if userinput_name == "0":
        load()
    else:
        #create player
        player1 = const_entity(userinput_name, 1, 3, 0)

        #print player summary
        print("{0}'s XP is {1}".format(player1.name, player1.xp))
        print(player1)

        #write stats
        write_stats(player1)

        #wait 3 seconds
        print("Please Wait For 3 Seconds...")
        print("------------------------")
        time.sleep(3)
        
        #clear screen
        clear()

        #pass player to menu
        menu(player1)

turn_based()