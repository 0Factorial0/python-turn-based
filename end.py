def end():
    import time
    #import additional functions
    try:
        from additional_functions.clear import clear
        from additional_functions.delay_print import delay_print
    except:
        print("Import Error.")

    #clear screen
    clear()

    #ending text
    print("------------------------")
    delay_print("You Win!", 1)
    time.sleep(2)
    delay_print("The Game Is Over.", 1)
    time.sleep(2)
    print("")
    delay_print("Now You Can Go Out.", 1)
    time.sleep(2)
    print("")
    delay_print("Congrats...", 1)
    time.sleep(2)
    print("")
    for i in range(1,100):
        delay_print("The End.", 1)
        time.sleep(2)
        print("")
    print("------------------------")