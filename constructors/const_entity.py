def const_entity(name, lvl, x, xp):
    class Entity:
        #define entity object
        def __init__(self, name, lvl, x, xp):
            #import additional functions
            try:
                from additional_functions.define_object import define_object
            except:
                print("Import Error.")
            #pass to define function
            return define_object(self, name, lvl, x, xp)

        def __str__(self):
            #import additional functions
            try:
                from additional_functions.print_summary import print_summary
            except:
                print("Import Error.")
            #pass self to print function
            return print_summary(self)
    return Entity(name, lvl, x, xp)