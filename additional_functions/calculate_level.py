def calculate_level(player1):
    #import additional functions
    try:
        from constructors.const_entity import const_entity   
    except:
        print("Import Error.")

    #is level up
    flag = 0
    
    #check levels
    if player1.lvl == 1:
        if player1.xp >= 10:
            player1.lvl = 2
    elif player1.lvl == 2:
        if player1.xp >= 25:
            player1.lvl = 3
    elif player1.lvl == 3:
        if player1.xp >= 50:
            player1.lvl = 4
    elif player1.lvl == 4:
        if player1.xp >= 100:
            player1.lvl = 5
    elif player1.lvl == 5:
        if player1.xp >= 200:
            player1.lvl = 6
    else:
        print("Level Error.")
    
    #if level is up, reconstruct the player
    if flag == 1:
        print("Player Level Up")
        player2 = const_entity.Player(player1.name, player1.lvl+1, player1.x+1)
        player1 = player2
        print("Now {0} Is At Level {1}".format(player1.name, player1.lvl))

    return player1
