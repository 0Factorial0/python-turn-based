def write_stats(player1):
    import os
    #get path
    file_path = os.path.dirname(os.path.abspath("turn_based.py"))

    #get txt files
    name_file = open(file_path+"/player/name.txt", "w")
    level_file = open(file_path+"/player/level.txt", "w")
    strength_file = open(file_path+"/player/strength.txt", "w")
    agility_file = open(file_path+"/player/agility.txt", "w")
    hp_file = open(file_path+"/player/hp.txt", "w")

    #write stats
    name_file.write(str(player1.name))
    level_file.write(str(player1.lvl))
    strength_file.write(str(player1.attack))
    agility_file.write(str(player1.defence))
    hp_file.write(str(player1.hp))

    #close files
    name_file.close()
    level_file.close()
    strength_file.close()
    agility_file.close()
    hp_file.close()

def write_xp(player1):
    import os
    #get path
    file_path = os.path.dirname(os.path.abspath("turn_based.py"))

    #write xp
    xp_file = open(file_path+"/player/xp.txt", "w")
    xp_file.write(str(player1.xp))
    xp_file.close()