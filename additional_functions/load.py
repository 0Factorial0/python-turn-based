def load():
    import os

    #import additional functions
    try:
        from constructors.const_entity import const_entity
        from menu import menu
    except:
        print("Import Error.")

    #get path
    file_path = os.path.dirname(os.path.abspath("turn_based.py"))
    
    #get txt files
    name_file = open(file_path+"/player/name.txt", "r")
    level_file = open(file_path+"/player/level.txt", "r")
    xp_file = open(file_path+"/player/xp.txt", "r")

    #set player
    name = name_file.read()
    lvl = int(level_file.read())

    #create player
    player1 = const_entity(name, lvl, 3, int(xp_file.read()))

    #close files
    name_file.close()
    level_file.close()

    menu(player1)
