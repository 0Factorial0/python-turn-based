def delay_print(text, next):

    import time
    import sys

    for character in text:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.03)

    if next == 0:
        pass
    else:
        print("")
