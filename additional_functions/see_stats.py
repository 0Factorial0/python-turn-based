def see_stats(player1):
    import time

    #import additional functions
    try:
        from additional_functions.clear import clear
        from menu import menu
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #print header
    print("------------------------")
    print("Character Sheet")
    print("------------------------")

    #print player
    print("{0}'s XP is {1}".format(player1.name, player1.xp))
    print(player1)

    #wait 5 seconds
    print("Please Wait For 5 Seconds...")
    print("------------------------")
    time.sleep(5)
    #clear screen
    clear()

    #pass player to menu
    menu(player1)
