def rest(player1):
    import os

    #import additional functions
    try:
        from menu import menu
    except:
        print("Import Error.")

    #get path
    file_path = os.path.dirname(os.path.abspath("turn_based.py"))
    
    #get txt files
    name_file = open(file_path+"/player/name.txt", "r")
    strength_file = open(file_path+"/player/strength.txt", "r")
    agility_file = open(file_path+"/player/agility.txt", "r")
    hp_file = open(file_path+"/player/hp.txt", "r")
    level_file = open(file_path+"/player/level.txt", "w")
    xp_file = open(file_path+"/player/xp.txt", "w")

    #set player
    player1.name = name_file.read()
    player1.attack = int(strength_file.read())
    player1.defence = int(agility_file.read())
    player1.hp = int(hp_file.read())
    level_file.write(str(player1.lvl))
    xp_file.write(str(player1.xp))

    #close files
    name_file.close()
    level_file.close()
    strength_file.close()
    agility_file.close()
    hp_file.close()
    xp_file.close()

    menu(player1)