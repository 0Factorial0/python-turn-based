#define object
def define_object(self, name, lvl, x, xp):
    self.name = name
    self.lvl = lvl
    self.x = x
    self.attack = 2 * lvl * self.x
    self.defence = 2 * lvl
    self.hp = 2 * 3 * lvl * self.x
    self.xp = xp
    self.dice = 4 * lvl *  self.x
    self.exist = True
